package si.uni_lj.fri.pbd.miniapp1;


import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;

import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.ListFragment;

import com.google.gson.Gson;

import java.util.ArrayList;


public class ContactsFragment extends ListFragment {
    static final int REQUEST_IMAGE_CAPTURE = 1;
    private static final String SHARED_PREFS = "sharedPrefs";
    private static final String IMAGEKEY = "imageKey";
    private static final String ARRAYKEY ="arraykey";
    private static final String EMPTY = "empty";
    ListView l1;
    String[] neki = {"2","6","9"};
    ArrayList<Contact> allContacts = new ArrayList<Contact>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_contacts,container,false);
    }

    @Override
    public void onSaveInstanceState(Bundle outState){
        super.onSaveInstanceState(outState);
    }



    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ArrayList<String> arrayList = new ArrayList<>();
        ArrayAdapter arrayAdapter = new ArrayAdapter(getActivity(),android.R.layout.simple_list_item_checked,arrayList);
        setListAdapter(arrayAdapter);


        ContentResolver contentResolver = getActivity().getContentResolver();
        Cursor cursor = contentResolver.query(ContactsContract.Contacts.CONTENT_URI,null,null,null,null);
        if(cursor.moveToFirst()){
            do{
                Contact contact = new Contact();

                String name =(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME_PRIMARY)));
                arrayList.add(name);
                contact.setName(name);
                String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));


                Cursor cursor2 = contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                        new String[] {id}, null);
                if(cursor2.moveToFirst()){
                    String number = cursor2.getString(cursor2.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DATA));
                    //arrayList.add(number);
                    contact.setNumber(number);
                }



                Cursor cursor3 = contentResolver.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null, ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?",
                        new String[] {id}, null);
                if(cursor3.moveToFirst()){
                    String email = cursor3.getString(cursor3.getColumnIndex(ContactsContract.CommonDataKinds.Email.ADDRESS));
                    //arrayList.add(email);
                    contact.setEmail(email);
                }

                allContacts.add(contact);
            }while(cursor.moveToNext());
            arrayAdapter.notifyDataSetChanged();
        }





        //setListAdapter(new ArrayAdapter<String>(getActivity(), R.layout.support_simple_spinner_dropdown_item,getResources().getStringArray(R.array.titles)));
    }




    @Override
    public void onDestroyView() {
        super.onDestroyView();
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        //TODO :Arraylist to string

        Gson gson = new Gson();
        String json = gson.toJson(allContacts);

        String temp = "SHAREDPREF";
        editor.putString(ARRAYKEY,json);
        editor.apply();
    }



    @Override
    public void onListItemClick(ListView l, View v, int pos, long id) {
        super.onListItemClick(l, v, pos, id);
        allContacts.get(pos).toggleChecked();
        String status;
        if(allContacts.get(pos).getChecked()){
            status = "checked";
        }else{
            status = "not checked";
        }
        Toast.makeText(getActivity(), "Item "+pos+" is "  + status, Toast.LENGTH_SHORT).show();
        CheckedTextView ctv = (CheckedTextView) v;
        ctv.toggle();
    }
}
