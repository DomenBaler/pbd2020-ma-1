package si.uni_lj.fri.pbd.miniapp1;

public class Contact {
    private String name;
    private String email;
    private String number;
    public boolean checked;

    public Contact(){
    }

    public Contact(String name, String email, String number){
        this.name = name;
        this.email = email;
        this.number = number;
    }

    public String getName(){
        return name;
    }

    public String getEmail(){
        return email;
    }

    public String getNumber(){
        return number;
    }

    public boolean getChecked(){
        return checked;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setEmail(String email){
        this.email = email;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }
    public void toggleChecked(){
        checked = !checked;
    }

}
