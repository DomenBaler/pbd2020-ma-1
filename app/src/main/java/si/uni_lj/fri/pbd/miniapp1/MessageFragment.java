package si.uni_lj.fri.pbd.miniapp1;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;

public class MessageFragment extends Fragment {
    static final int REQUEST_IMAGE_CAPTURE = 1;
    private static final String SHARED_PREFS = "sharedPrefs";
    private static final String IMAGEKEY = "imageKey";
    private static final String ARRAYKEY ="arraykey";
    private static final String EMPTY = "empty";


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_message,container,false);
    }



    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);



        getActivity().findViewById(R.id.button1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedPreferences = getActivity().getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE);
                String arrayString = sharedPreferences.getString(ARRAYKEY,EMPTY);


                Gson gson = new Gson();
                ArrayList<String> email = new ArrayList<String>();
                ArrayList<String> number = new ArrayList<String>();

                if(!arrayString.equals(EMPTY)){
                    ArrayList<Contact> allContacts = gson.fromJson(arrayString, new TypeToken<ArrayList<Contact>>(){}.getType());
                    //int size = allContacts.size();
                    //String toto = String.valueOf(size);
                    //Toast.makeText(getActivity(), allContacts.get(0).toString(), Toast.LENGTH_SHORT).show();
                    for(int i = 0; i < allContacts.size(); i++){
                        Contact contact = allContacts.get(i);
                        if(contact.getChecked() && contact.getEmail() !=  null){
                            email.add(contact.getEmail());
                        }
                    }
                    if(email.size() > 0){
                        String[] targetEmails = new String[email.size()];
                        targetEmails = email.toArray(targetEmails);




                        //Toast.makeText(getActivity(),"dela",Toast.LENGTH_LONG).show();
                        Intent emailIntent = new Intent(Intent.ACTION_SEND);
                        emailIntent.setData(Uri.parse("mailto:"));
                        emailIntent.setType("text/plain");

                        emailIntent.putExtra(Intent.EXTRA_EMAIL  , targetEmails);
                        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "subject");
                        emailIntent.putExtra(Intent.EXTRA_TEXT   , "Message Body");

                        try{
                            startActivity(Intent.createChooser(emailIntent,"Send email"));
                        }catch(android.content.ActivityNotFoundException ex){
                            Toast.makeText(getActivity(), "There is no email client installed.", Toast.LENGTH_SHORT).show();
                        }
                    }else{
                        Toast.makeText(getActivity(), "There are no contacts with email selected.", Toast.LENGTH_SHORT).show();
                    }


                }else{

                    Toast.makeText(getActivity(), "There are no contacts with email selected.", Toast.LENGTH_SHORT).show();
                }







            }
        });

        getActivity().findViewById(R.id.button2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedPreferences = getActivity().getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE);
                String arrayString = sharedPreferences.getString(ARRAYKEY,EMPTY);


                Gson gson = new Gson();
                ArrayList<String> email = new ArrayList<String>();
                ArrayList<String> number = new ArrayList<String>();

                if(!arrayString.equals(EMPTY)){
                    ArrayList<Contact> allContacts = gson.fromJson(arrayString, new TypeToken<ArrayList<Contact>>(){}.getType());
                    int size = allContacts.size();
                    String toto = String.valueOf(size);

                    for(int i = 0; i < allContacts.size(); i++){
                        Contact contact = allContacts.get(i);
                        if(contact.getChecked() && contact.getNumber() !=  null){
                            number.add(contact.getNumber());
                        }
                    }
                    if(number.size() > 0){

                        String targetNumbers ="";
                        for(int i = 0; i < number.size()-1; i++){
                            targetNumbers = targetNumbers+number.get(i)+";";
                        }targetNumbers = targetNumbers+number.get(number.size()-1);

                        //Toast.makeText(getActivity(), targetNumbers, Toast.LENGTH_SHORT).show();

                        Intent smsIntent = new Intent(Intent.ACTION_SENDTO,Uri.parse("smsto:"+targetNumbers));
                        smsIntent.putExtra("sms_body", "Hello World");
                        startActivity(smsIntent);

                    }else{
                        Toast.makeText(getActivity(), "There are no contacts with number selected.", Toast.LENGTH_SHORT).show();
                    }


                }else{

                    Toast.makeText(getActivity(), "There are no contacts with number selected.", Toast.LENGTH_SHORT).show();
                }



            }
        });

    }
}
